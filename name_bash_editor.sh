# hello_program
# make a program that says hello
# topics covered:
    # arguments
    # variables
    # interpolation

# Description:
# make a bash script that takes in 1 argument that is name and says hello to that person. 
# we should assing the argument a meaningful variable name like person_name

# I want to capture one argument, when the script is called

# echo $1

# assign the variable person_name to the first incoming argument
echo $person_name
person_name=$1

# I want to echo back hello + the name of the person 
echo "Hello $person_name!"

