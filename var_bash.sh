## Bash script for testing Arguments and User Input 

# Escape characters example

#echo "\$1"
#echo "\" \" "

# Also single quotes protect double quotes and double quotes protect single quotes

#echo "I'm a cool guy"
#echo 'This is a double quotes """""""""""""'


# Argument 
# data that a function can take in and use internally
# $ touch <argument>
# or 
# $ mv <argument1> <argument2>

# in script is you want to use arguments they are define with $#, where # is a number.
# for example 
echo $arg1
echo $3
echo $1
echo $2

echo "first time i print arg1: $arg1"

# don't like $1 it is not descriptive, i'll assiing it to a variable called arg1
arg1=$1
echo $arg1



# Interpolation of variable into a strings

echo "this is argument 1: $1" 
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "this is argument 1: $4" " will you break?" 



# To call this script with 4 arguments, just call the script and pass each argument seprated by spaces.
# syntax
# ./var_bash.sh hi filipe James Kofi Amalia


