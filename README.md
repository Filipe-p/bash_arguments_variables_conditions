# Bash Script, Variables, Arguments and conditions

This will be a small demo / code along for use to learn how to use variables, arguments and conditions.

By then end we'll have a small script that uses these to install ngix on a ubunto machine when given an IP as a argument.

We'll talk about the difference between arguments and variables.

We will also look into using conditions, to help us run our script - using control flow and handling errors.

## To cover

- Script basics
- running script
- escape notations 
- Arguments
- Variables and user input 
- Conditions


### Script basics

Script is several bash command written in a file that a bash interperter can execute. 
This is useful to automate tasks and others. 

### running script

To run a script, it must first be executable. Check it's permission using `ll` or `ls -l`. It should have `x`to be executed.

Add the permission "execute" by `chmod +x <file>`

Youm might need to to this in a remote machine as well when moving or creating new files on the fly. 

You can run the file by just pointing to it or calling `bash <file>

```bash
# just pointing to file
./vas_bash.sh

# to run the fille remotele call it using bas
bash ./var_bash.sh

# the dot just represent "HERE", you could put the entire path to file:
/Users/filipepaiva/code/cohort9/week2/bash_variables/var_bash.sh 

```

### Escape notation

In Bash and other language, you have certain character that behave/signal important things other than the actual simbol. For example `""` are used to outline a string of character. 

How do you print out/ echo out some `"`?

**you user what is a called an escape character. In bash this is the \ backslash**

```bash
#example 
# Escape characters example

echo "\$1"
echo "\" \" "

# Also single quotes protect double quotes and double quotes protect single quotes

echo "I'm a cool guy"
echo 'This is a double quotes """""""""""""'

```


### Arguments in scripts

Arguments are data that a function can take in and use internally.

`$ touch <argument>`
or
`$ mv <argument1> <argument2>`

In script is you want to use arguments they are define with `$#`, where # is a number representing the numerical order in which the argument was passed to the script.

for example

```bash
echo $1
echo $1 $2


# Interpolation of variable into a strings

echo "this is argument 1: $1" 
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "this is argument 1: $4" " will you break?" 

```

Imagine you want to make a function, or script, that takes in any number of arguments and does something?

You can use `$*` to represent all the given arguments in a list.

To explain the above we need loops!

### Loops

A loop is a block of code that run for a fiven number of times.
This can be useful if you have repetetive tasks.

```bash

for x in [item item2 item3]
    do
        echo "Block of code"
        echo $x
    done
```

In a loop, a program iterates of the iteratable object (usually a list), substituting in each iteration the x for an object on the list, until there are no more object.




### Conditions
