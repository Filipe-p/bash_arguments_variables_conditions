# Now I want a program that will say hello to any number of people I want.

# It must capture any number or arguments 
# It should say hello

## topics are:
    # how to capture any number of arguments
    # loops to iterate until all arguments have been handled

# to capture any areguments we might want touse $*
# we'll need a for loop to iterate over this 

for arg in $*
do
    echo "HEllo $arg!"
done


