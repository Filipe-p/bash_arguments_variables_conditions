# hello_program Version 2 🌮
# make a program that says hello to 3 people at once (😊)

# description
# the program will receive 3 names, and say hello to all 3.
# We should use good variables names: first_person, secon_person, third_person

# psudo code
# I want to capture 3 arguments and ressing them to their variable names
# I want the variable  first_person to be assigne to the first argument
# I want the variable  second_person to be assigne to the second argument
first_person=$1
second_person=$2
third_person=$3

# I want to call the variable names and say hello
echo "HELLO THERE $first_person!"
echo "HELLO THERE $second_person!"
echo "HELLO THERE $third_person!"
