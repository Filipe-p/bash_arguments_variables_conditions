# Condition evaluate if something is results in true or false.
# This allows us to control flow - meaning our code with take different actions depending on this codition

# syntax 
# if ((<condition>))
# then
    # block of code
# else 
    # Block of code
# fi

#example 

COUNTER=0

if (($COUNTER < 5 ))
then
    echo "counter is smaller than 5!"
else
    echo "counter is larger than 5"
fi

# user input is gathered with read 
echo "give me a number"
read NUMBER 
if (($NUMBER < 5 ))
then
    echo "NUMBER is smaller than 5!"
else
    echo "NUMBER is larger than 5"
fi

